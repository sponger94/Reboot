﻿using Autofac;
using Reboot.Abstractions;
using Reboot.Abstractions.Factories;

namespace Reboot.Core.Factories
{
    public class ViewModelFactory : IViewModelFactory
    {
        private readonly IContainer _container;

        public ViewModelFactory(IContainer container)
        {
            _container = container;
        }

        public INavigationableViewModel GetViewModel<T>()
        {
            return _container.Resolve<T>() as INavigationableViewModel;
        }

        public INavigationableViewModel GetMainViewModel<T>(string platform)
        {
            return _container.ResolveNamed<T>(platform) as INavigationableViewModel;
        }
    }
}
