﻿using System.Threading.Tasks;
using ReactiveUI;
using Reboot.Abstractions;
using Reboot.Abstractions.Facades;
using Reboot.Abstractions.Factories;
using Reboot.Abstractions.Services;

namespace Reboot.Core.Services
{
    public class NavigationService : INavigationService
    {
        private readonly INavigationFacade _navigationFacade;
        private readonly IPlatformFacade _platformFacade;
        private readonly IViewFactory _viewFactory;
        private readonly IViewModelFactory _viewModelFactory;

        public NavigationService(
            IViewModelFactory viewModelFactory,
            IViewFactory viewFactory,
            INavigationFacade navigationFacade,
            IPlatformFacade platformFacade)
        {
            _viewModelFactory = viewModelFactory;
            _viewFactory = viewFactory;
            _navigationFacade = navigationFacade;
            _platformFacade = platformFacade;
        }

        public IViewFor CurrentView { get; private set; }

        public Task NavigateAsync(INavigationableViewModel viewModel)
        {
            CurrentView = _viewFactory.ResolveView(viewModel);
            return _navigationFacade.PushAsync(CurrentView);
        }

        public Task NavigateAsync<T>()
        {
            var viewModel = _viewModelFactory.GetViewModel<T>();
            CurrentView = _viewFactory.ResolveView(viewModel);
            return _navigationFacade.PushAsync(CurrentView);
        }

        public Task NavigateModalAsync(INavigationableViewModel viewModel)
        {
            CurrentView = _viewFactory.ResolveView(viewModel);
            return _navigationFacade.PushModalAsync(CurrentView);
        }

        public Task NavigateModalAsync<T>()
        {
            var viewModel = _viewModelFactory.GetViewModel<T>();
            CurrentView = _viewFactory.ResolveView(viewModel);
            return _navigationFacade.PushModalAsync(CurrentView);
        }

        public Task CloseModalAsync(bool animated)
        {
            return _navigationFacade.PopModalAsync(animated);
        }

        public Task NavigateToMainPage(INavigationableViewModel viewModel)
        {
            CurrentView = _viewFactory.ResolveView(viewModel);
            return _navigationFacade.NavigateToMainPage(CurrentView);
        }

        public Task NavigateToMainPage<T>()
        {
            var viewModel = _viewModelFactory.GetViewModel<T>();
            CurrentView = _viewFactory.ResolveView(viewModel);
            return _navigationFacade.NavigateToMainPage(CurrentView);
        }

        public Task NavigateToMainPageContent(INavigationableViewModel viewModel)
        {
            CurrentView = _viewFactory.ResolveView(viewModel);
            return _navigationFacade.NavigateToMainPageContent(CurrentView);
        }

        public Task NavigateToRoot()
        {
            return _navigationFacade.NavigateToRoot();
        }
    }
}
