﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reboot.Abstractions.Facades
{
    public interface IPlatformFacade
    {
        string RuntimePlatform { get; }

        string Android { get; }

        string iOS { get; }

        string UWP { get; }
    }
}
