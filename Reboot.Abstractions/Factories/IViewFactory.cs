﻿using ReactiveUI;

namespace Reboot.Abstractions.Factories
{
    public interface IViewFactory
    {
        IViewFor ResolveView(INavigationableViewModel vm);

        IViewFor ResolveView(INavigationableViewModel vm, string name);
    }
}
