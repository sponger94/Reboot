﻿namespace Reboot.Abstractions.Factories
{
    public interface IViewModelFactory
    {
        INavigationableViewModel GetViewModel<T>();

        INavigationableViewModel GetMainViewModel<T>(string platform);
    }
}
