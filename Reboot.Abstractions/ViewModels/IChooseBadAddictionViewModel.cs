﻿using System.Windows.Input;

namespace Reboot.Abstractions.ViewModels
{
    public interface IChooseBadAddictionViewModel : INavigationableViewModel
    {
        int AddictionType { get; set; }

        ICommand Next { get; }
    }
}
