﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Reboot.Abstractions.ViewModels
{
    public interface IDashboardViewModel : INavigationableViewModel
    {
        ICommand HelpMe { get; }

        ICommand Accident { get; }
    }
}
