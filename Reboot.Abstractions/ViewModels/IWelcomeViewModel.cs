﻿using System.Windows.Input;

namespace Reboot.Abstractions.ViewModels
{
    public interface IWelcomeViewModel
    {
        ICommand Next { get; }
    }
}
