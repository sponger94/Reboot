﻿namespace Reboot.Abstractions
{
    public interface INavigationableViewModel
    {
        string Title { get; }
    }
}
