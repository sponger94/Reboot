﻿using System.Threading.Tasks;
using ReactiveUI;

namespace Reboot.Abstractions.Services
{
    public interface INavigationService
    {
        IViewFor CurrentView { get; }

        Task NavigateAsync(INavigationableViewModel viewModel);

        Task NavigateAsync<T>(); //TODO: Put where clause to <T>

        Task NavigateModalAsync(INavigationableViewModel viewModel);

        Task NavigateModalAsync<T>();  //TODO: Put where clause to <T>

        Task CloseModalAsync(bool animated);

        Task NavigateToMainPage(INavigationableViewModel viewModel);

        Task NavigateToMainPage<T>();  //TODO: Put where clause to <T>

        Task NavigateToMainPageContent(INavigationableViewModel viewModel);

        Task NavigateToRoot();
    }
}
